package com.pms.mlq.repository;

import java.math.BigInteger;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pms.common.entity.PMSContractQuotationDt;
import com.pms.common.entity.PMSContractQuotationDtPk;

public interface PMSContractQuotationDtRepository
		extends JpaRepository<PMSContractQuotationDt, PMSContractQuotationDtPk> {
	@Transactional
	@Modifying
	@Query("delete from PMSContractQuotationDt q where q.pk.contractQuotationId=:contractQuotationId")
	void deleteByQuotationId(@Param("contractQuotationId") BigInteger contractQuotationId);
	
	



}
