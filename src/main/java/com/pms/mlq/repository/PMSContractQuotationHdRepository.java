package com.pms.mlq.repository;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.pms.common.entity.PMSContractQuotationHd;
import com.pms.common.entity.PMSContractQuotationHdPk;
import com.pms.deliveryorder.common.entity.PMSDeliveryOrderHd;

public interface PMSContractQuotationHdRepository
		extends JpaRepository<PMSContractQuotationHd, PMSContractQuotationHdPk> {

	@Query("select q from PMSContractQuotationHd q  where q.trxDate between :startDate and :endDate  "
			+ " and q.admContractCategory.contractCategoryId = Case when :contractCategoryId > java.math.BigInteger.ZERO then :contractCategoryId else q.admContractCategory.contractCategoryId end  " +
			" and q.admBarge.bargeId = Case when :bargeId > java.math.BigInteger.ZERO then :bargeId else q.admBarge.bargeId end  " +
			" and q.processStatusDesc = Case when :processStatusDesc != '' then :processStatusDesc else q.processStatusDesc end   " + 
			" and q.admVendorId.vendorId = Case When :vendorId > java.math.BigInteger.ZERO then :vendorId Else q.admVendorId.vendorId end  order by q.createDate,q.pk.contractQuotationId desc")
	List<PMSContractQuotationHd> findAllQuotationsByDateBetween(Date startDate, Date endDate, BigInteger bargeId, BigInteger vendorId, BigInteger contractCategoryId, String processStatusDesc);

	@Query("select q from PMSContractQuotationHd q  where q.trxDate between :startDate and :endDate "
			+ " and q.admContractCategory.contractCategoryId = Case when :contractCategoryId > java.math.BigInteger.ZERO then :contractCategoryId else q.admContractCategory.contractCategoryId end  " +
			" and q.admBarge.bargeId = Case when :bargeId > java.math.BigInteger.ZERO then :bargeId else q.admBarge.bargeId end  " +
			" and q.processStatusDesc = Case when :processStatusDesc != '' then :processStatusDesc else q.processStatusDesc end   " + 
			" and q.admVendorId.vendorId = Case When :vendorId > java.math.BigInteger.ZERO then :vendorId Else q.admVendorId.vendorId end  order by q.createDate,q.pk.contractQuotationId desc")
	List<PMSContractQuotationHd> findAllQuotationsByDateBetweenWithBargeId(Date startDate, Date endDate, BigInteger bargeId, BigInteger vendorId, BigInteger contractCategoryId, String processStatusDesc );

	@Query("SELECT e from PMSContractQuotationHd e where e.pk.contractQuotationId=:contractQuotationId and e.admBarge.bargeId=:bargeId   order by e.pk.contractQuotationId asc")
	Optional<PMSContractQuotationHd> findMplSupplierContractIdAndBargeId(BigInteger contractQuotationId,
			BigInteger bargeId);

	@Query("SELECT e from PMSContractQuotationHd e where e.pk.contractQuotationId=:contractQuotationId and e.admVendorId.vendorId=:vendorId   order by e.pk.contractQuotationId asc")
	Optional<PMSContractQuotationHd> findMplcontractQuotationIdAndVendorId(BigInteger contractQuotationId,
			BigInteger vendorId);

	@Query("SELECT e from PMSContractQuotationHd e where e.pk.contractQuotationId=:contractQuotationId   order by e.pk.contractQuotationId asc")
	Optional<PMSContractQuotationHd> findMlqcontractQuotationId(BigInteger contractQuotationId);

	@Query("SELECT e from PMSContractQuotationHd e where e.pk.contractQuotationNo=:contractQuotationNo and e.admBarge.bargeId=:bargeId   order by e.pk.contractQuotationId asc")
	Optional<PMSContractQuotationHd> findMplSupplierContractNoAndBargeId(String contractQuotationNo,
			BigInteger bargeId);

	@Query("SELECT e from PMSContractQuotationHd e where e.pk.contractQuotationNo=:contractQuotationNo and e.admVendorId.vendorId=:vendorId   order by e.pk.contractQuotationId asc")
	Optional<PMSContractQuotationHd> findMqlContractQuotationNoAndVendorId(String contractQuotationNo,
			BigInteger vendorId);

	@Query("SELECT e from PMSContractQuotationHd e where e.pk.contractQuotationNo=:contractQuotationNo   order by e.pk.contractQuotationId asc")
	Optional<PMSContractQuotationHd> findMlqContractQuotationNo(String contractQuotationNo);
	
// ************************************************** Email Service Block Start ************************************************************//

	/**
	 * For Find Master List quotation  based on @param
	 */
	/**
	 
	 * @param contractQuotationId
	 * @return
	 */
	@Query("SELECT e from PMSContractQuotationHd e where  e.pk.contractQuotationId=:contractQuotationId  order by e.pk.contractQuotationId desc")
	Optional<PMSContractQuotationHd> findContractQuotation( BigInteger contractQuotationId);

	/**
	 * For updateStatus based @param
	 */

	/**
	 * @param isSubmitted
	 * @param submittedBy
	 * @param submittedDate
	 * @param contractQuotationId
	 * @return
	 */
	@Modifying
	@Transactional
	@Query("UPDATE PMSContractQuotationHd SET isSubmitted = :isSubmitted, submittedBy = :submittedBy, submittedDate =:submittedDate WHERE  pk.contractQuotationId=:contractQuotationId ")
	public int updateStatus(@Param("isSubmitted") int isSubmitted, @Param("submittedBy") String submittedBy,
			@Param("submittedDate") Timestamp submittedDate, @Param("contractQuotationId") BigInteger contractQuotationId);

// ************************************************** Email Service Block End ************************************************************///

}
