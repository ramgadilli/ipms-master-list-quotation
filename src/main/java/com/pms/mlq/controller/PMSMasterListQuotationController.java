package com.pms.mlq.controller;

import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pms.common.controller.BaseController;
import com.pms.common.exception.HeaderException;
import com.pms.common.pojo.PMSContractQuotationHdPojo;
import com.pms.mlq.service.PMSMasterListQuotationService;


@RestController
@RequestMapping("/masterListQuotation")
public class PMSMasterListQuotationController extends BaseController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PMSMasterListQuotationController.class);

	@Autowired
	PMSMasterListQuotationService mplService;
	
	
	
	
	@PostMapping(value = "/getMasterListQuotationByDateRange", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getMasterListQuotationByDateRange(@RequestHeader Map<String, String> headers,@RequestBody Map<String,String> map) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			validateRequestBodyDateRange(map);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = mplService.getMasterListQuotationByDateRange(headers, map);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterListQuotationController.getMasterListQuotationByDateRange "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));

		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}

	@PostMapping(value = "/saveMasterListQuotation", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveMasterListQuotation(@RequestHeader Map<String, String> headers,
			@RequestBody PMSContractQuotationHdPojo req) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();

			resp = mplService.saveMLQ(headers, req);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterListQuotation.MasterListQuotation "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));

		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
	
	@PutMapping(value = "/editMasterListQuotation/{contractQuotationId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> editMasterListQuotation(@RequestHeader Map<String, String> headers,
			@PathVariable BigInteger contractQuotationId, @RequestBody PMSContractQuotationHdPojo req) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);

			if (!contractQuotationId.equals(req.getContractQuotationId())) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body("Contract QuotationId (In Master List Quotation Object) Is Not Matched With Path Value ");
			}

			long startCurrentTimeMiles = System.currentTimeMillis();

			resp = mplService.editMpl(headers, req);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterListQuotation.editMasterPriceList "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}
//
	@GetMapping(value = "/getMasterListQuotation/{contractQuotationId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getMasterListQuotation(@RequestHeader Map<String, String> headers,
			@PathVariable BigInteger contractQuotationId) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = mplService.getMpl(headers, contractQuotationId);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterListQuotation.getMasterListQuotation "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}

	@GetMapping(value = "/getMasterListQuotationByNo/{contractQuotationNo}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getMasterListQuotationByNo(@RequestHeader Map<String, String> headers,
			@PathVariable String contractQuotationNo) throws Exception {

		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);
			long startCurrentTimeMiles = System.currentTimeMillis();
			resp = mplService.getMplByNo(headers, contractQuotationNo);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterListQuotation.getMasterListQuotationByNo "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}


	@PutMapping(value = "/changeMasterListQuotationStatus/{contractQuotationId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> changeMasterListQuotationStatus(@RequestHeader Map<String, String> headers,
			@PathVariable BigInteger contractQuotationId, @RequestBody Map<String, String> status) throws Exception {
		ResponseEntity<?> resp = null;
		try {
			validateRequestHeader(headers);

			long startCurrentTimeMiles = System.currentTimeMillis();

			resp = mplService.changeMasterListQuotationStatus(headers, contractQuotationId, status);
			long endCurrentTimeMiles = System.currentTimeMillis();

			LOGGER.debug("Total time took for PMSMasterListQuotation.changeMasterListQuotationStatus "
					+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
		} catch (HeaderException hEx) {
			resp = returnHeaderException(hEx);
		}
		return resp;
	}

	//************************************************** Email Service Block Start ************************************************************//

		/**
		 * @param headers
		 * @param bodyMap
		 * @return
		 * @throws Exception
		 */
		@PostMapping(value = "/submitConfirmation", produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<?> submitConfirmation(@RequestHeader Map<String, String> headers,
				@RequestBody Map<String, String> bodyMap) throws Exception {

			ResponseEntity<?> resp = null;
			try {
				validateRequestHeader(headers);
				long startCurrentTimeMiles = System.currentTimeMillis();
				resp = mplService.updateStatus(headers, bodyMap);
				long endCurrentTimeMiles = System.currentTimeMillis();

				LOGGER.debug("Total time took for PMSMasterListQuotation.submitConfirmation "
						+ TimeUnit.MILLISECONDS.toMillis(endCurrentTimeMiles - startCurrentTimeMiles));
			} catch (HeaderException hEx) {
				resp = returnHeaderException(hEx);
			}
			return resp;
		}

	//************************************************** Email Service Block End ************************************************************//

}
