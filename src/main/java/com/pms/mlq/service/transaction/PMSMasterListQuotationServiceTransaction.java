package com.pms.mlq.service.transaction;

import java.math.BigInteger;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.pms.common.constants.CommonConstants;
import com.pms.common.constants.ISMSErrorMessages;
import com.pms.common.entity.AdmBarge;
import com.pms.common.entity.AdmContractCategory;
import com.pms.common.entity.AdmEmployee;
import com.pms.common.entity.AdmMainCode;
import com.pms.common.entity.AdmPMSStatus;
import com.pms.common.entity.AdmSubCode;
import com.pms.common.entity.AdmUOM;
import com.pms.common.entity.AdmVendor;
import com.pms.common.entity.AdmVesselDepartment;
import com.pms.common.entity.PMSContractQuotationDt;
import com.pms.common.entity.PMSContractQuotationDtPk;
import com.pms.common.entity.PMSContractQuotationHd;
import com.pms.common.entity.PMSContractQuotationHdPk;
import com.pms.common.pojo.PMSContractQuotationDtPojo;
import com.pms.common.pojo.PMSContractQuotationHdPojo;
import com.pms.common.service.transaction.BaseServiceTrans;
import com.pms.common.utilities.DateUtilities;
import com.pms.common.utilities.Utilities;
import com.pms.common.utilities.UtilitiesiSMSError;
import com.pms.mlq.repository.PMSContractQuotationDtRepository;
import com.pms.mlq.repository.PMSContractQuotationHdRepository;



@Service
public class PMSMasterListQuotationServiceTransaction {
	private static final Logger LOGGER = LoggerFactory.getLogger(PMSMasterListQuotationServiceTransaction.class);

@Autowired
PMSContractQuotationHdRepository contractQuotationHdRepository;
@Autowired
PMSContractQuotationDtRepository contractQuotationDtRepository;


	@Autowired
	BaseServiceTrans baseSerTrans;
	
	List<PMSContractQuotationHdPojo> mlqListPjo = new ArrayList<PMSContractQuotationHdPojo>();
	
	//PMSContractQuotationHd contractQuotationHd=new PMSContractQuotationHd();
	//PMSContractQuotationHdPk contractQuotationHdPk=new PMSContractQuotationHdPk();
	
	//PMSContractQuotationDt contractQuotationDt=new PMSContractQuotationDt();
	//List<PMSContractQuotationDt> mplDtList= new ArrayList<PMSContractQuotationDt>();
	//PMSContractQuotationDtPk contractQuotationDtPk=new PMSContractQuotationDtPk();
	
	
	private void convertEntToPojo(PMSContractQuotationHd ent, PMSContractQuotationHdPojo mplHdPojo, boolean isList) {

		BeanUtils.copyProperties(ent, mplHdPojo);
		mplHdPojo.setContractQuotationId(ent.getPk().getContractQuotationId());
		mplHdPojo.setContractQuotationNo(ent.getPk().getContractQuotationNo());
		mplHdPojo.setBargeId(ent.getAdmBarge().getBargeId());
		mplHdPojo.setVendorId(ent.getAdmVendorId().getVendorId());
		mplHdPojo.setStatusId(ent.getAdmPMSStatus().getPmsStatusId());
		mplHdPojo.setEmployeeId(ent.getAdmEmployee().getEmployeeId());
		mplHdPojo.setVesseldepartmentId(ent.getAdmVesselDepartment().getVesselDepartmentId());
		mplHdPojo.setContractCategoryId(ent.getAdmContractCategory().getContractCategoryId());
		mplHdPojo.setContractCategoryCode(ent.getAdmContractCategory().getContractCategoryCode());
		mplHdPojo.setContractCategoryName(ent.getAdmContractCategory().getContractCategoryName());
		if (!isList) {
			for (PMSContractQuotationDt mplDt : ent.getContractQuotationDetails()) {
				PMSContractQuotationDtPojo mplDtPojo = new PMSContractQuotationDtPojo();
				BeanUtils.copyProperties(mplDt, mplDtPojo);
				mplDtPojo.setContractQuotationId(mplDt.getPk().getContractQuotationId());
				mplDtPojo.setContractQuotationNo(mplDt.getPk().getContractQuotationNo());
				mplDtPojo.setItemNo(mplDt.getPk().getItemNo());
				
				mplDtPojo.setUomId(mplDt.getAdmUOM().getUomId());
				mplDtPojo.setUomCode(mplDt.getAdmUOM().getUomCode());
				mplDtPojo.setUomName(mplDt.getAdmUOM().getUomName());
				mplDtPojo.setMainCodeId(mplDt.getMainCodeId().getMainCodeId());
				mplDtPojo.setMainCode(mplDt.getMainCodeId().getMainCode());
				mplDtPojo.setMainCodeDescription(mplDt.getMainCodeId().getMainCodeDescription());
				mplDtPojo.setSubcodeId(mplDt.getAdmSubCode().getSubCodeId());
				mplHdPojo.addContractSupplierPriceDetails(mplDtPojo);
			}
		} else {
			mplHdPojo.setContractQuotationDetails(null);
		}
	}



	@Transactional
	public List<PMSContractQuotationHdPojo> getMasterPriceListByDateRange(Map<String, String> headers, Map<String, String> map) {
		LOGGER.info("Received getMasterPriceListByDateRangeForBackOffice  ");
		List<PMSContractQuotationHd> contractQuotationHdList = null;
		mlqListPjo = new ArrayList<PMSContractQuotationHdPojo>();
		PMSContractQuotationHdPojo pojo = null;
		
		int transactionId = 0;
		int moduleId = 0;
		int roleId = 0;
		BigInteger ownerId = BigInteger.ZERO;
		Date startDate = null;
		Date endDate = null;
		BigInteger bargeId = BigInteger.ZERO;
		BigInteger vendorId = BigInteger.ZERO;
		BigInteger contractCategoryId = BigInteger.ZERO;
		String processStatusDesc = "";
		BigInteger headerObjectId = BigInteger.ZERO;
		
		try {
			
			transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
			moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
			roleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.ROLE_ID)));
			headerObjectId = new BigInteger(String.valueOf(headers.get(CommonConstants.OBJECT_ID)));
			
			startDate = Date.valueOf(map.get("startDate"));
			endDate = Date.valueOf(map.get("endDate"));
			bargeId = map.containsKey("bargeId") ? new BigInteger(String.valueOf(map.get("bargeId"))) : BigInteger.ZERO;
			vendorId = map.containsKey("vendorId") ? new BigInteger(String.valueOf(map.get("vendorId"))) : BigInteger.ZERO;
			contractCategoryId = map.containsKey("contractCategoryId") ? new BigInteger(String.valueOf(map.get("contractCategoryId"))) : BigInteger.ZERO;
			processStatusDesc = map.containsKey("processStatusDesc") ? (String)map.get("processStatusDesc") : "";
			processStatusDesc = processStatusDesc == null ? "" : processStatusDesc;
			
			java.sql.Date futureDate = Utilities.addDays(endDate, 1);
			
			if (roleId == CommonConstants.ROLE_ID_BACK_OFFICE) {
				contractQuotationHdList = contractQuotationHdRepository.findAllQuotationsByDateBetween(startDate, futureDate, bargeId, vendorId, contractCategoryId, processStatusDesc);
			} else if (roleId == CommonConstants.ROLE_ID_VESSEL) {
				contractQuotationHdList = contractQuotationHdRepository.findAllQuotationsByDateBetweenWithBargeId(startDate, futureDate,headerObjectId, vendorId, contractCategoryId, processStatusDesc);
			}
			
			if (contractQuotationHdList != null && contractQuotationHdList.size() > 0) {
				for (PMSContractQuotationHd contractSupplierList : contractQuotationHdList) {
					pojo = new PMSContractQuotationHdPojo();
					convertEntToPojo(contractSupplierList, pojo, true);
					mlqListPjo.add(pojo);
				}

			}

		} catch (Exception ex) {
			throw ex;
		}
		return mlqListPjo;
	}
	
	/*
	@Transactional
	public List<PMSContractQuotationHdPojo> getMasterPriceListByDateRangeForRole2(Date startDate, Date endDate, BigInteger bargeId) {
		LOGGER.info("Received getMasterPriceListByDateRangeForRole2");
		List<PMSContractQuotationHd> contractQuotationHdList = null;
		mlqListPjo = new ArrayList<PMSContractQuotationHdPojo>();
		PMSContractQuotationHdPojo pojo = null;
		try {
			contractQuotationHdList = contractQuotationHdRepository.findAllQuotationsByDateBetweenWithBargeId(startDate, endDate,bargeId);
			if (contractQuotationHdList != null && contractQuotationHdList.size() > 0) {
				for (PMSContractQuotationHd contractSupplierList : contractQuotationHdList) {
					pojo = new PMSContractQuotationHdPojo();
					convertEntToPojo(contractSupplierList, pojo, false);
					mlqListPjo.add(pojo);
				}

			}

		} catch (Exception ex) {
			throw ex;
		}
		return mlqListPjo;
	}
	*/


	@Transactional
public PMSContractQuotationHdPojo  saveMLQ(Map<String, String> headers, PMSContractQuotationHdPojo req)
		throws Exception {
		
	boolean transactionSuccess = false;
	Map<String, Object> map = null;
	BigInteger outNewBtsId = null;
	String outNewDocCode = null;
	PMSContractQuotationHd contractQuotationHd=new PMSContractQuotationHd();
	BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
	int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
	int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
	String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));
	PMSContractQuotationDtPk contractQuotationDtPk=new PMSContractQuotationDtPk();
	PMSContractQuotationDt contractQuotationDt=new PMSContractQuotationDt();
	PMSContractQuotationHdPk contractQuotationHdPk=new PMSContractQuotationHdPk();
	int intNewEditVersion = 0;

	try {
		Date currentDate = new Date(DateUtilities.getCurrentDate().getTime());
		map = baseSerTrans.getGeneratedSeqNo(req.getBranchId(), moduleId, transactionId,
				CommonConstants.BANK_ID_ZERO, currentDate, CommonConstants.IN_PREFIX, 0);
		outNewBtsId = new BigInteger(map.get("outNewBtsId").toString());
		outNewDocCode = map.get("outNewDocCode").toString();
		req.setContractQuotationId(outNewBtsId);
		req.setContractQuotationNo(outNewDocCode);
		req.setModuleId(moduleId);
		req.setTransactionId(transactionId);
		BeanUtils.copyProperties(req, contractQuotationHd);
		
		
		contractQuotationHdPk=new PMSContractQuotationHdPk(req.getContractQuotationId(), req.getContractQuotationNo());
		contractQuotationHd.setPk(contractQuotationHdPk);
		contractQuotationHd.setAdmBarge(new AdmBarge(req.getBargeId()));
		contractQuotationHd.setAdmVendorId(new AdmVendor(req.getVendorId()));
		contractQuotationHd.setAdmPMSStatus(new AdmPMSStatus(new BigInteger("3")));
		contractQuotationHd.setAdmEmployee(new AdmEmployee(req.getEmployeeId() == null ? "" : req.getEmployeeId().toString()));
		contractQuotationHd.setAdmVesselDepartment(new AdmVesselDepartment(req.getVesseldepartmentId()));
		contractQuotationHd.setAdmContractCategory(new AdmContractCategory(req.getContractCategoryId()));
		contractQuotationHd.setContractQuoteDate(contractQuotationHd.getTrxDate());
		contractQuotationHd.setCreateDate(DateUtilities.getCurrentDate());
		contractQuotationHd.setCreateBy(userCode);
		contractQuotationHd.setModuleId(moduleId);
		contractQuotationHd.setTransactionId(transactionId);
		//contractQuotationHd.setEditDate(currentDateTime);
		contractQuotationHd.setTrxDate(DateUtilities.getCurrentDate());
		
		intNewEditVersion = baseSerTrans.getNewEditVersion(req.getBranchId(), moduleId, transactionId, req.getContractQuotationId(), "SAVE");
		contractQuotationHd.setEditVersion(intNewEditVersion);
		
		if(contractQuotationHd.getEditVersion() < 0 ) {
			throw new Exception( "EditVersion calculculation went incorrect, please check. EditVersion: " + contractQuotationHd.getEditVersion());
		}
		
		LOGGER.info("Created New Supplier Contract Id :- " + outNewBtsId + " & New Supplier Contract No :- " + outNewDocCode);

		for (PMSContractQuotationDtPojo reqDetails : req.getContractQuotationDetails()) {
			
			
			contractQuotationDt = new PMSContractQuotationDt();
			contractQuotationDtPk = new PMSContractQuotationDtPk(outNewBtsId, outNewDocCode,reqDetails.getItemNo());
			BeanUtils.copyProperties(reqDetails, contractQuotationDt);
			contractQuotationDt.setPk(contractQuotationDtPk);
			contractQuotationDt.setAdmUOM(new AdmUOM(reqDetails.getUomId()));
			contractQuotationDt.setMainCodeId(new AdmMainCode(reqDetails.getMainCodeId()));
			contractQuotationDt.setAdmSubCode(new AdmSubCode(reqDetails.getSubCodeId()));
			reqDetails.setSerialNo(Integer.parseInt(reqDetails.getItemNo()));
			contractQuotationDt.setSerialNo(Integer.parseInt(reqDetails.getItemNo()));
			contractQuotationHd.addContractSupplierPriceDetails(contractQuotationDt);
		}
		contractQuotationHdRepository.saveAndFlush(contractQuotationHd);
		
		 baseSerTrans.updateProcessStatusDescInHD( branchId,  moduleId,  transactionId, outNewBtsId,  CommonConstants.MASTER_LIST_QUOTATION_TABLE_NAME,  "Open");
		transactionSuccess = true;
	} catch (Exception e) {
		throw e;
	} finally {
		if (transactionSuccess) {
			baseSerTrans.saveAuditLogs(branchId, userCode, moduleId, transactionId,
					CommonConstants.MASTER_LIST_QUOTATION_TABLE_NAME, outNewDocCode, ""+req.getVendorId(), outNewBtsId + "",
					"Create", req.getBargeId() + "");

		}
	}
	return req;
}
	
	@Transactional
	public PMSContractQuotationHdPojo  editMpl(Map<String, String> headers, PMSContractQuotationHdPojo req)
			throws Exception {
			
		boolean transactionSuccess = false;
		BigInteger outNewBtsId = null;
		String outNewDocCode = null;
		PMSContractQuotationHd contractQuotationHd=new PMSContractQuotationHd();
		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));
		List<PMSContractQuotationDt> mplDtList= new ArrayList<PMSContractQuotationDt>();
		PMSContractQuotationDtPk contractQuotationDtPk=new PMSContractQuotationDtPk();
		PMSContractQuotationDt contractQuotationDt=new PMSContractQuotationDt();
		PMSContractQuotationHdPk contractQuotationHdPk=new PMSContractQuotationHdPk();
		int intNewEditVersion = 0;

		try {
			//baseSerTrans.createHistoryRecord(req.getBranchId(), moduleId, transactionId, req.getContractQuotationId());
			BeanUtils.copyProperties(req, contractQuotationHd);
			
			req.setModuleId(moduleId);
			req.setTransactionId(transactionId);
			contractQuotationHdPk=new PMSContractQuotationHdPk(req.getContractQuotationId(), req.getContractQuotationNo());
			contractQuotationHd.setPk(contractQuotationHdPk);
			contractQuotationHd.setAdmBarge(new AdmBarge(req.getBargeId()));
			contractQuotationHd.setAdmVendorId(new AdmVendor(req.getVendorId()));
			contractQuotationHd.setAdmPMSStatus(new AdmPMSStatus(req.getStatusId()));
			contractQuotationHd.setAdmEmployee(new AdmEmployee(req.getEmployeeId() == null ? "" : req.getEmployeeId().toString()));
			contractQuotationHd.setAdmVesselDepartment(new AdmVesselDepartment(req.getVesseldepartmentId()));	
			contractQuotationHd.setModuleId(moduleId);
			contractQuotationHd.setTransactionId(transactionId);
			contractQuotationHd.setEditDate(DateUtilities.getCurrentDate());
			contractQuotationHd.setEditBy(userCode);
			intNewEditVersion = baseSerTrans.getNewEditVersion(req.getBranchId(), moduleId, transactionId, req.getContractQuotationId(), "EDIT");
			contractQuotationHd.setEditVersion(intNewEditVersion);
			
			if(contractQuotationHd.getEditVersion() < 0 ) {
				throw new Exception( "EditVersion calculculation went incorrect, please check. EditVersion: " + contractQuotationHd.getEditVersion());
			}
			
			LOGGER.info("Existing  Contract Quotation Id :- " + req.getContractQuotationId() + " & Existing  Contract Quotation No :- " + req.getContractQuotationNo());

			for (PMSContractQuotationDtPojo reqDetails : req.getContractQuotationDetails()) {
				
				contractQuotationDt = new PMSContractQuotationDt();
				contractQuotationDtPk = new PMSContractQuotationDtPk(req.getContractQuotationId(), req.getContractQuotationNo(),reqDetails.getItemNo());
				BeanUtils.copyProperties(reqDetails, contractQuotationDt);
				contractQuotationDt.setPk(contractQuotationDtPk);
				
				contractQuotationDt.setAdmUOM(new AdmUOM(reqDetails.getUomId()));
				contractQuotationDt.setMainCodeId(new AdmMainCode(reqDetails.getMainCodeId()));
				contractQuotationDt.setAdmSubCode(new AdmSubCode(reqDetails.getSubCodeId()));
				contractQuotationDt.setSerialNo(Integer.parseInt(reqDetails.getItemNo()));
				reqDetails.setSerialNo(Integer.parseInt(reqDetails.getItemNo()));
				contractQuotationHd.addContractSupplierPriceDetails(contractQuotationDt);
				contractQuotationDtRepository.deleteByQuotationId(req.getContractQuotationId());
			
				mplDtList.add(contractQuotationDt);
			}
			
			contractQuotationDtRepository.deleteByQuotationId(contractQuotationHd.getPk().getContractQuotationId());
			contractQuotationHdRepository.saveAndFlush(contractQuotationHd);
			LOGGER.info("request to editMlq for Master List Quotation : " + req.getBargeId());
			transactionSuccess = true;
		} catch (Exception e) {
			throw e;
		} finally {
			if (transactionSuccess) {
				baseSerTrans.saveAuditLogs(branchId, userCode, moduleId, transactionId,
						CommonConstants.MASTER_LIST_QUOTATION_TABLE_NAME, outNewDocCode, ""+req.getVendorId(), outNewBtsId + "",
						"Edit", req.getBargeId() + "");

			}
		}
		return req;
	}
	
	
	public Optional<PMSContractQuotationHd> getMpl(Map<String, String> headers, BigInteger quotationId) {
		Optional<PMSContractQuotationHd> hd = null;
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		int roleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.ROLE_ID)));
		BigInteger ownerId = new BigInteger(String.valueOf(headers.get(CommonConstants.OBJECT_ID)));
		try {
			LOGGER.info("getMlq for Master List Quotation: " + quotationId);

			if (roleId == CommonConstants.ROLE_ID_BACK_OFFICE) {
				hd = contractQuotationHdRepository.findMlqcontractQuotationId(quotationId);
			} else if (roleId == CommonConstants.ROLE_ID_VESSEL) {
				hd = contractQuotationHdRepository.findMplSupplierContractIdAndBargeId(quotationId, ownerId);
			} else if (roleId == CommonConstants.ROLE_ID_VENDOR) {
				hd = contractQuotationHdRepository.findMplcontractQuotationIdAndVendorId(quotationId, ownerId);
			}

		} catch (Exception e) {
			throw e;
		}
		return hd;
	}
	
	public Optional<PMSContractQuotationHd> getMplByNo(Map<String, String> headers, String quotationNo) {
		Optional<PMSContractQuotationHd> hd = null;
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		int roleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.ROLE_ID)));
		BigInteger ownerId = new BigInteger(String.valueOf(headers.get(CommonConstants.OBJECT_ID)));
		try {
			LOGGER.info("getMpl for Master List Quotation : " + quotationNo);

			if (roleId == CommonConstants.ROLE_ID_BACK_OFFICE) {
				hd = contractQuotationHdRepository.findMlqContractQuotationNo(quotationNo);
			} else if (roleId == CommonConstants.ROLE_ID_VESSEL) {
				hd = contractQuotationHdRepository.findMplSupplierContractNoAndBargeId(quotationNo, ownerId);
			} else if (roleId == CommonConstants.ROLE_ID_VENDOR) {
				hd = contractQuotationHdRepository.findMqlContractQuotationNoAndVendorId(quotationNo, ownerId);
			}

		} catch (Exception e) {
			throw e;
		}
		return hd;
	}

	//************************************************** Email Service Block Start ************************************************************//

		/**
		 * @param headers
		 * @param contractQuotationId
		 * @return
		 */
		public Optional<PMSContractQuotationHd> findContractQuotation(Map<String, String> headers, BigInteger contractQuotationId) {
			Optional<PMSContractQuotationHd> hd = null;
			try {
				LOGGER.info("findContractQuotation for ContractQuotation : " + contractQuotationId);
				hd = contractQuotationHdRepository.findContractQuotation(contractQuotationId);

			} catch (Exception e) {
				throw e;
			}
			return hd;
		}

		/**
		 * @param headers
		 * @param contractQuotationId
		 * @return
		 */
		public int updateStatus(Map<String, String> headers, BigInteger contractQuotationId) {
			int hd;
			String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));
			try {
				LOGGER.info("updateStatus for ContractQuotation : " + contractQuotationId);
				hd = contractQuotationHdRepository.updateStatus(1, userCode, DateUtilities.getCurrentDate(), 
						contractQuotationId);

			} catch (Exception e) {
				throw e;
			}
			return hd;
		}

	//************************************************** Email Service Block End ************************************************************//	

	
	@Transactional
	public ResponseEntity<?> changeMasterListQuotationStatus(Map<String, String> headers, BigInteger supplierContractId, Map<String, String> bodyHeader) throws Exception {
		boolean transactionSuccess = false;

		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));
		String strCancelRemarks = String.valueOf(bodyHeader.get("cancelRemarks"));
		String strRejectRemarks = String.valueOf(bodyHeader.get("rejectRemarks"));
		
		String trxNo = "";
		String referenceNo = "";
		BigInteger bargeid = BigInteger.ZERO;
		
		Map<String, String> isCancelCheck = headers;
		BigInteger oldStatus = BigInteger.ZERO;
		BigInteger newStatus = BigInteger.ZERO;
		String mlqStatus = bodyHeader.get("status");

		try {
			LOGGER.info("request to deleteMlq for Master List Quotation : " + supplierContractId);
			Optional<PMSContractQuotationHd> hdRec = contractQuotationHdRepository.findMlqcontractQuotationId(supplierContractId);
			if (hdRec.isPresent()) {
				
				oldStatus = hdRec.get().getAdmPMSStatus().getPmsStatusId();
				
				isCancelCheck.put("inOldStatus", oldStatus.toString());
				isCancelCheck.put("inIsCancel", hdRec.get().getIsCancel() + "");
				isCancelCheck.put("documentId", hdRec.get().getPk().getContractQuotationId().toString());
				String strCancelErrorMsg = baseSerTrans.isEligibleToCancel(isCancelCheck);
				
				if ( !strCancelErrorMsg.contentEquals("") ) {
					if( strCancelErrorMsg.contains("Exception") ) {
						return UtilitiesiSMSError.returnResponse(HttpStatus.INTERNAL_SERVER_ERROR, null, ISMSErrorMessages.ISMSRESPONSECODE_1000, ISMSErrorMessages.ERROR_EXCEPTION, strCancelErrorMsg);
					}
					return UtilitiesiSMSError.returnResponse(HttpStatus.OK, null, ISMSErrorMessages.ISMSRESPONSECODE_100, strCancelErrorMsg, null);
				}
				
				newStatus = Utilities.getNewStatusValue(mlqStatus);
				
				if(newStatus.compareTo(BigInteger.ZERO) == 0) {
					return UtilitiesiSMSError.returnResponse(HttpStatus.OK, null, ISMSErrorMessages.ISMSRESPONSECODE_100, ISMSErrorMessages.NO_ACTION_PARAMETERS_ISSUE, "Please send valid process status code.");
				}
				
				if(newStatus.compareTo(CommonConstants.PMS_STATUS_CANCEL) == 0) {
					hdRec.get().setIsCancel(1);
					hdRec.get().setCancelBy(userCode);
					hdRec.get().setCancelDate(DateUtilities.getCurrentDate());
					hdRec.get().setCancelRemarks(strCancelRemarks);
					hdRec.get().setRejectRemarks(strRejectRemarks);
	
					hdRec.get().setAdmPMSStatus(new AdmPMSStatus(CommonConstants.PMS_STATUS_CANCEL));
					hdRec.get().setProcessStatusDesc(CommonConstants.PROCESSSTATUSDESC_CANCEL);
					
					bargeid = hdRec.get().getAdmBarge().getBargeId();
					
					//baseSerTrans.updateProcessStatusDescInHD( branchId,  moduleId,  transactionId, supplierContractId,  CommonConstants.MASTER_LIST_QUOTATION_TABLE_NAME,  "Cancel");
				}
				
				if (oldStatus.compareTo(newStatus) == -1) {
					hdRec.get().setAdmPMSStatus(new AdmPMSStatus(newStatus));
					contractQuotationHdRepository.saveAndFlush(hdRec.get());
					baseSerTrans.callProcSrUpdatePMSStatus(branchId, moduleId, transactionId, newStatus, hdRec.get().getPk().getContractQuotationId());
					transactionSuccess = true;
				} else {
					transactionSuccess = false;
				}
				
				/*
				if( hdRec.get().getIsCancel() == 1 ) {
					CustomResponse resp = new CustomResponse(HttpStatus.OK, null,
							new ResponseException(HttpStatus.OK, "Master List Quotation already cancelled: " + supplierContractId));
					return ResponseEntity.status(HttpStatus.OK).body(resp);
				} else {
					hdRec.get().setIsCancel(1);
					hdRec.get().setCancelBy(userCode);
					hdRec.get().setCancelDate(DateUtilities.getCurrentDate());
					hdRec.get().setCancelRemarks(strCancelRemarks);
					hdRec.get().setRejectRemarks(strRejectRemarks);
	
					hdRec.get().setAdmPMSStatus(new AdmPMSStatus(CommonConstants.PMS_STATUS_CANCEL));
					
					bargeid = hdRec.get().getAdmBarge().getBargeId();
					contractQuotationHdRepository.saveAndFlush(hdRec.get());
					transactionSuccess = true;
					baseSerTrans.updateProcessStatusDescInHD( branchId,  moduleId,  transactionId, supplierContractId,  CommonConstants.MASTER_LIST_QUOTATION_TABLE_NAME,  "Cancel");
				}
				*/
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (transactionSuccess) {
				baseSerTrans.saveAuditLogs(branchId, userCode, moduleId, transactionId,
						CommonConstants.MASTER_LIST_QUOTATION_TABLE_NAME, trxNo, referenceNo, supplierContractId + "",
						"Cancel", bargeid + "");
			}
		}
		return UtilitiesiSMSError.returnResponse(HttpStatus.OK, null, ISMSErrorMessages.ISMSRESPONSECODE_100, ISMSErrorMessages.SUCCESS_CHANGE_STATUS, null);
	}

	
}
