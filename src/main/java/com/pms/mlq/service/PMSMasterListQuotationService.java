package com.pms.mlq.service;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pms.common.apicall.ApiCallService;
import com.pms.common.constants.CommonConstants;
import com.pms.common.entity.PMSContractQuotationDt;
import com.pms.common.entity.PMSContractQuotationHd;
import com.pms.common.exception.ResponseException;
import com.pms.common.pojo.BTSApprovalProcessPojo;
import com.pms.common.pojo.PMSContractQuotationDtPojo;
import com.pms.common.pojo.PMSContractQuotationHdPojo;
import com.pms.common.response.CustomResponse;
import com.pms.common.service.BaseService;
import com.pms.common.service.transaction.BaseServiceTrans;
import com.pms.common.utilities.DateUtilities;
import com.pms.mlq.repository.PMSContractQuotationHdRepository;
import com.pms.mlq.service.transaction.PMSMasterListQuotationServiceTransaction;

@Service
public class PMSMasterListQuotationService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PMSMasterListQuotationService.class);
	@Autowired
	BaseServiceTrans baseSerTrans;

	@Autowired
	BaseService baseSer;
	@Autowired
	PMSContractQuotationHdRepository contractQuotationHdRepository;

	@Autowired
	PMSMasterListQuotationServiceTransaction mplTransaction;

	//PMSContractQuotationHdPojo cntractQuotationHdPojo = new PMSContractQuotationHdPojo();

	//PMSContractQuotationDtPojo cntractQuotationDtPojo = new PMSContractQuotationDtPojo();
 
	public ResponseEntity<?> getMasterListQuotationByDateRange(Map<String, String> headers, Map<String, String> map)
			throws Exception {
		List<PMSContractQuotationHdPojo> quotattionHdList = null;
		int roleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.ROLE_ID)));
		BigInteger ownerId = new BigInteger(String.valueOf(headers.get(CommonConstants.OBJECT_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		Date startDate = Date.valueOf(map.get("startDate"));
		Date endDate = Date.valueOf(map.get("endDate"));
		try {

			quotattionHdList = mplTransaction.getMasterPriceListByDateRange(headers, map);
			
			/*if (roleId == 1) {
				quotattionHdList = mplTransaction.getMasterPriceListByDateRangeForBackOffice(startDate,
						Utilities.addDays(endDate, 1), moduleId);
			} else if (roleId == 2) {
				quotattionHdList = mplTransaction.getMasterPriceListByDateRangeForRole2(startDate,
						Utilities.addDays(endDate, 1), ownerId);
			}*/

		} catch (Exception ex) {

			LOGGER.error("We have received the error " + ex.getMessage());
			String errorMessage  = baseSerTrans.saveErrorLog(headers, ex, "PMSMasterListQuotationService");
			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage ));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, quotattionHdList, null);
		return ResponseEntity.status(HttpStatus.OK).body(resp);
	}

	private PMSContractQuotationHdPojo conertDataFromEntToPojo(PMSContractQuotationHd hd) {
		PMSContractQuotationHdPojo cntractQuotationHdPojo = new PMSContractQuotationHdPojo();
		PMSContractQuotationDtPojo cntractQuotationDtPojo = new PMSContractQuotationDtPojo();

		BeanUtils.copyProperties(hd, cntractQuotationHdPojo);
		cntractQuotationHdPojo.setContractQuotationId(hd.getPk().getContractQuotationId());
		cntractQuotationHdPojo.setContractQuotationNo(hd.getPk().getContractQuotationNo());
		cntractQuotationHdPojo.setBargeId(hd.getAdmBarge().getBargeId());
		cntractQuotationHdPojo.setVendorId(hd.getAdmVendorId().getVendorId());
		cntractQuotationHdPojo.setStatusId(hd.getAdmPMSStatus().getPmsStatusId());
		cntractQuotationHdPojo.setEmployeeId(hd.getAdmEmployee().getEmployeeId());
		cntractQuotationHdPojo.setVesseldepartmentId(hd.getAdmVesselDepartment().getVesselDepartmentId());
		cntractQuotationHdPojo.setContractCategoryId(hd.getAdmContractCategory().getContractCategoryId());
		cntractQuotationHdPojo.setContractCategoryCode(hd.getAdmContractCategory().getContractCategoryCode());
		cntractQuotationHdPojo.setContractCategoryName(hd.getAdmContractCategory().getContractCategoryName());
  
		if (hd.getContractQuotationDetails() != null && hd.getContractQuotationDetails().size() > 0) {

			for (PMSContractQuotationDt details : hd.getContractQuotationDetails()) {
				PMSContractQuotationDtPojo mplDtPojo = new PMSContractQuotationDtPojo();
				BeanUtils.copyProperties(details, mplDtPojo);
				mplDtPojo.setContractQuotationId(details.getPk().getContractQuotationId());
				mplDtPojo.setContractQuotationNo(details.getPk().getContractQuotationNo());
				mplDtPojo.setItemNo(details.getPk().getItemNo());
				
				mplDtPojo.setUomId(details.getAdmUOM().getUomId());
				mplDtPojo.setUomCode(details.getAdmUOM().getUomCode());
				mplDtPojo.setUomName(details.getAdmUOM().getUomName());
				mplDtPojo.setMainCodeId(details.getMainCodeId().getMainCodeId());
				mplDtPojo.setMainCode(details.getMainCodeId().getMainCode());
				mplDtPojo.setMainCodeDescription(details.getMainCodeId().getMainCodeDescription());
				
				mplDtPojo.setSubcodeId(details.getAdmSubCode().getSubCodeId());
				mplDtPojo.setSubCode(details.getAdmSubCode().getSubCode());
				mplDtPojo.setSubCodeDescription(details.getAdmSubCode().getSubCodeDescription());
				cntractQuotationHdPojo.addContractSupplierPriceDetails(mplDtPojo);
			}
		}
		return cntractQuotationHdPojo;
	}

	public ResponseEntity<?> saveMLQ(Map<String, String> headers, PMSContractQuotationHdPojo req) {
		PMSContractQuotationHdPojo cntractQuotationHdPojo = new PMSContractQuotationHdPojo();
		try {
			cntractQuotationHdPojo = mplTransaction.saveMLQ(headers, req);
			
			//placeMLQRecForApproval(headers, req);	
			

		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			String errorMessage  = baseSerTrans.saveErrorLog(headers, e, "MasterListQuotation");
			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage ));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, cntractQuotationHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}
	@Value("${ipms.hostName}")
	private String ipmsHostName;

	@Value("${ipms.port}")
	private String ipmsPort;
	@Value("${ipms.approvalProcess.module}")
	private String approvalModulePath;
	
	@Value("${ipms.approvalProcess.context}")
	private String ipmsapprovalProcessContext;
	
	private void placeMLQRecForApproval(Map<String, String> headers, PMSContractQuotationHdPojo pojo) throws JsonProcessingException {
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		String insertRecordOperation = "createNewVersionedRecord";
		Date currentDate = new Date(DateUtilities.getCurrentDate().getTime());
		final String approvalServiceURL = ipmsHostName + ":" + ipmsPort + ipmsapprovalProcessContext + approvalModulePath + "/"
				+ insertRecordOperation;
		
		ObjectMapper mapper = new ObjectMapper();
		BTSApprovalProcessPojo approvalProcessDetails = new BTSApprovalProcessPojo();
		
		approvalProcessDetails.setApprovalStatusId(1);
		approvalProcessDetails.setApproveBy(String.valueOf(headers.get(CommonConstants.USER_CODE)));
		approvalProcessDetails.setApproveDate(pojo.getCreateDate());
		approvalProcessDetails.setBargeId(pojo.getBargeId());
		approvalProcessDetails.setBrageName("");
		approvalProcessDetails.setBranchId(pojo.getBranchId());
		approvalProcessDetails.setCreateBy(String.valueOf(headers.get(CommonConstants.USER_CODE)));
		approvalProcessDetails.setCreateDate(DateUtilities.getCurrentDate());
		approvalProcessDetails.setDocumentId(pojo.getContractQuotationId());
		approvalProcessDetails.setDocumentNo(pojo.getContractQuotationNo());
		approvalProcessDetails.setModuleId(moduleId);
		approvalProcessDetails.setTransactionId(transactionId);
		approvalProcessDetails.setForwardBy(String.valueOf(headers.get(CommonConstants.USER_CODE)));
		//TODO 
		approvalProcessDetails.setForwardBy_UserGroupId(new BigInteger("3"));
		approvalProcessDetails.setForwardDate(DateUtilities.getCurrentDate());
		//TODO
		approvalProcessDetails.setForwardTo_UserGroupId(new BigInteger("3"));
		approvalProcessDetails.setForwardToEmployeeId(new BigInteger("13308050002"));
		approvalProcessDetails.setRemarks("");
		
		approvalProcessDetails.setTrxDate(DateUtilities.getCurrentDate());
		List<BTSApprovalProcessPojo> jsonList = new ArrayList<BTSApprovalProcessPojo>();
		jsonList.add(approvalProcessDetails);
		
		String json = mapper.writeValueAsString(jsonList);
		
		 LOGGER.info("request String Approval process :: "+json);
		CustomResponse response = ApiCallService.callService(approvalServiceURL, HttpMethod.POST, json,createHttpHeadersForRequistion(headers));
		LOGGER.info("Placed a MLQ rec for approval process - status " +response.getStatus().value());
		
	}
	
	private static HttpHeaders createHttpHeadersForRequistion(Map<String, String> inHeaders) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		BigInteger branchId = new BigInteger(String.valueOf(inHeaders.get(CommonConstants.BRANCH_ID)));
		BigInteger categoryId = new BigInteger(String.valueOf(inHeaders.get(CommonConstants.CATEGORY_ID)));
		BigInteger moduleId = new BigInteger(String.valueOf(inHeaders.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(inHeaders.get(CommonConstants.USER_CODE));
		int roleId = Integer.parseInt(String.valueOf(inHeaders.get(CommonConstants.ROLE_ID)));
		BigInteger ownerId = new BigInteger(String.valueOf(inHeaders.get(CommonConstants.OBJECT_ID)));

		headers.add("categoryId", categoryId + "");
		headers.add("branchId", branchId + "");
		headers.add("moduleId", moduleId + "");
		headers.add("transactionId",9+ "");
		headers.add("userCode", userCode);
		headers.add("roleId", roleId+"");
		headers.add("objectId", ownerId+"");

		return headers;
	}
	
	public ResponseEntity<?> editMpl(Map<String, String> headers, PMSContractQuotationHdPojo req) {
		PMSContractQuotationHdPojo cntractQuotationHdPojo = new PMSContractQuotationHdPojo();
		try {
			cntractQuotationHdPojo = mplTransaction.editMpl(headers, req);

		} catch (EntityNotFoundException entNotFound) {
			LOGGER.error("We have received the error " + entNotFound.getMessage());
			String errorMessage  = baseSerTrans.saveErrorLog(headers, entNotFound, "MasterListQuotation");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, entNotFound.getMessage()));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);

		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			String errorMessage  = baseSerTrans.saveErrorLog(headers, e, "MasterListQuotation");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage ));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);

		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, cntractQuotationHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}

	public ResponseEntity<?> getMpl(Map<String, String> headers, BigInteger reqId) {
		PMSContractQuotationHdPojo cntractQuotationHdPojo = new PMSContractQuotationHdPojo();
		Optional<PMSContractQuotationHd> hd = null;

		try {
			hd = mplTransaction.getMpl(headers, reqId);
			if (hd.isPresent()) {
				cntractQuotationHdPojo = conertDataFromEntToPojo(hd.get());
			} else {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested user"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			String errorMessage  = baseSerTrans.saveErrorLog(headers, e, "MasterListQuotation");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage ));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, cntractQuotationHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}

	public ResponseEntity<?> getMplByNo(Map<String, String> headers, String supplierNo) {
		PMSContractQuotationHdPojo cntractQuotationHdPojo = new PMSContractQuotationHdPojo();
		Optional<PMSContractQuotationHd> hd = null;

		try {
			hd = mplTransaction.getMplByNo(headers, supplierNo);
			if (hd.isPresent()) {
				cntractQuotationHdPojo = conertDataFromEntToPojo(hd.get());
			} else {
				CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
						new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested user"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
			}
		} catch (Exception e) {
			LOGGER.error("We have received the error " + e.getMessage());
			String errorMessage  = baseSerTrans.saveErrorLog(headers, e, "MasterListQuotation");

			CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
					new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage ));
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
		CustomResponse resp = new CustomResponse(HttpStatus.OK, cntractQuotationHdPojo, null);
		return ResponseEntity.ok().body(resp);
	}

	@Transactional
	public ResponseEntity<?> changeMasterListQuotationStatus(Map<String, String> headers, BigInteger supplierContractId, Map<String, String> bodyHeader) throws Exception {
		boolean transactionSuccess = false;

		BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
		int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
		int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
		String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));
		String strCancelRemarks = String.valueOf(bodyHeader.get("cancelRemarks"));
		String strRejectRemarks = String.valueOf(bodyHeader.get("rejectRemarks"));
		
		String trxNo = "";
		String referenceNo = "";
		BigInteger bargeid = BigInteger.ZERO;

		try {
			
			return mplTransaction.changeMasterListQuotationStatus(headers, supplierContractId, bodyHeader);
			
			/*
			LOGGER.info("request to deleteMlq for Master List Quotation : " + supplierContractId);
			Optional<PMSContractQuotationHd> hdRec = contractQuotationHdRepository
					.findMlqcontractQuotationId(supplierContractId);
			if (hdRec.isPresent()) {
				if( hdRec.get().getIsCancel() == 1 ) {
					CustomResponse resp = new CustomResponse(HttpStatus.OK, null,
							new ResponseException(HttpStatus.OK, "Master List Quotation already cancelled: " + supplierContractId));
					return ResponseEntity.status(HttpStatus.OK).body(resp);
				} else {
					hdRec.get().setIsCancel(1);
					hdRec.get().setCancelBy(userCode);
					hdRec.get().setCancelDate(DateUtilities.getCurrentDate());
					hdRec.get().setCancelRemarks(strCancelRemarks);
					hdRec.get().setRejectRemarks(strRejectRemarks);
	
					hdRec.get().setAdmPMSStatus(new AdmPMSStatus(CommonConstants.PMS_STATUS_CANCEL));
					
					bargeid = hdRec.get().getAdmBarge().getBargeId();
					contractQuotationHdRepository.saveAndFlush(hdRec.get());
					transactionSuccess = true;
					baseSerTrans.updateProcessStatusDescInHD( branchId,  moduleId,  transactionId, supplierContractId,  CommonConstants.MASTER_LIST_QUOTATION_TABLE_NAME,  "Cancel");
				}
			}
			*/
		} catch (Exception e) {
			throw e;
		} finally {
			if (transactionSuccess) {
				baseSerTrans.saveAuditLogs(branchId, userCode, moduleId, transactionId,
						CommonConstants.MASTER_LIST_QUOTATION_TABLE_NAME, trxNo, referenceNo, supplierContractId + "",
						"Cancel", bargeid + "");
			}
		}
		//CustomResponse resp = new CustomResponse(HttpStatus.OK, "Successfully Changed The Status for Master List Quotation  :: " + supplierContractId, null);
		//return ResponseEntity.status(HttpStatus.OK).body(resp);
	}
	
	
//************************************************** Email Service Block Start ************************************************************//

		ByteArrayOutputStream outputStream = null;
		@Value("${isms.email.url.suffix}")
		private String suffixKey;
		
		private void placeMLQRecForApproval(Map<String, String> headers,Timestamp createdDate, BigInteger bargeId,BigInteger branchId,BigInteger contractQuotationId,String contractQuotationNo ) throws JsonProcessingException {
			int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
			int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
			String insertRecordOperation = "createNewVersionedRecord";
			Date currentDate = new Date(DateUtilities.getCurrentDate().getTime());
			final String approvalServiceURL = ipmsHostName + ":" + ipmsPort + ipmsapprovalProcessContext + approvalModulePath + "/"
					+ insertRecordOperation;
			
			ObjectMapper mapper = new ObjectMapper();
			BTSApprovalProcessPojo approvalProcessDetails = new BTSApprovalProcessPojo();
			System.out.println("approvalServiceURL.."+approvalServiceURL);
			approvalProcessDetails.setApprovalStatusId(1);
			approvalProcessDetails.setApproveBy(String.valueOf(headers.get(CommonConstants.USER_CODE)));
			approvalProcessDetails.setApproveDate(createdDate);
			approvalProcessDetails.setBargeId(bargeId);
			approvalProcessDetails.setBrageName("");
			approvalProcessDetails.setBranchId(branchId);
			approvalProcessDetails.setCreateBy(String.valueOf(headers.get(CommonConstants.USER_CODE)));
			approvalProcessDetails.setCreateDate(DateUtilities.getCurrentDate());
			approvalProcessDetails.setDocumentId(contractQuotationId);
			approvalProcessDetails.setDocumentNo(contractQuotationNo);
			approvalProcessDetails.setModuleId(moduleId);
			approvalProcessDetails.setTransactionId(transactionId);
			approvalProcessDetails.setForwardBy(String.valueOf(headers.get(CommonConstants.USER_CODE)));
			//TODO 
			approvalProcessDetails.setForwardBy_UserGroupId(new BigInteger("2"));
			approvalProcessDetails.setForwardDate(DateUtilities.getCurrentDate());
			//TODO
			approvalProcessDetails.setForwardTo_UserGroupId(new BigInteger("2"));
			approvalProcessDetails.setForwardToEmployeeId(new BigInteger("13308050002"));
			approvalProcessDetails.setRemarks("");
			
			approvalProcessDetails.setTrxDate(DateUtilities.getCurrentDate());
			List<BTSApprovalProcessPojo> jsonList = new ArrayList<BTSApprovalProcessPojo>();
			jsonList.add(approvalProcessDetails);
			
			String json = mapper.writeValueAsString(jsonList);
			
			 LOGGER.info("request String Approval process :: "+json);
			CustomResponse response = ApiCallService.callService(approvalServiceURL, HttpMethod.POST, json,createHttpHeadersForRequistion(headers));
			LOGGER.info("Placed a MLQ rec for approval process - status " +response.getStatus().value());
			
		}

		/**
		 * @param headers
		 * @param bodyHeader
		 * @return
		 */
		public ResponseEntity<?> updateStatus(Map<String, String> headers, Map<String, String> bodyHeader) {
			int hdPojo;
			Optional<PMSContractQuotationHd> hd = null;
			BigInteger branchId = new BigInteger(String.valueOf(headers.get(CommonConstants.BRANCH_ID)));
			int transactionId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.TRANSACTION_ID)));
			int moduleId = Integer.parseInt(String.valueOf(headers.get(CommonConstants.MODULE_ID)));
			String userCode = String.valueOf(headers.get(CommonConstants.USER_CODE));

			BigInteger contractQuotationId = new BigInteger(String.valueOf(bodyHeader.get("contractQuotationId")));
			String contractQuotationNo = String.valueOf(bodyHeader.get("contractQuotationNo"));
			BigInteger vendorId = new BigInteger(String.valueOf(bodyHeader.get("vendorId")));

			try {
				hd = mplTransaction.findContractQuotation(headers,contractQuotationId);
				if (hd.isPresent()) {
					hdPojo = mplTransaction.updateStatus(headers, contractQuotationId);
					
					placeMLQRecForApproval(headers,DateUtilities.getCurrentDate(), BigInteger.ZERO,branchId,contractQuotationId,contractQuotationNo);

					baseSer.letsDoEmailJob(branchId, transactionId, moduleId, userCode, contractQuotationId, contractQuotationNo,
							DateUtilities.getCurrentDate(), vendorId, suffixKey.trim(), outputStream);
					
					 baseSerTrans.updateProcessStatusDescInHD( branchId,  moduleId,  transactionId, contractQuotationId,  CommonConstants.MASTER_LIST_QUOTATION_TABLE_NAME,  "submitted");

				} else {
					CustomResponse resp = new CustomResponse(HttpStatus.NOT_FOUND, null,
							new ResponseException(HttpStatus.NOT_FOUND, "No data found for requested user"));
					return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
				}
			} catch (Exception e) {
				LOGGER.error("We have received the error " + e.getMessage());
				String errorMessage  = baseSerTrans.saveErrorLog(headers, e, "MasterListQuotationSerive");

				CustomResponse resp = new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR, null,
						new ResponseException(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage ));
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
			}
			CustomResponse resp = new CustomResponse(HttpStatus.OK, "Successfully Submitted", null);
			return ResponseEntity.ok().body(resp);
		}

// ************************************************** Email Service Block End ************************************************************//	

}
